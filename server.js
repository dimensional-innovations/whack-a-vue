const express = require("express");
const app = express();
const serveStatic = require("serve-static");
const path = require("path");
const port = process.env.PORT || 80;
const server = require("http").Server(app);
const WebSocket = require("ws");
const wss = new WebSocket.Server({ server });

// A unique id for each new player
let uid = 0;

// An array to hold player objects
let players = [];

// Tell express to serve the "dist" directory
app.use(serveStatic(path.join(__dirname, "dist")));

server.listen(port, () => {
  console.log("Server Listening: http://localhost:" + port);
});

function addPlayer(id) {
  // If a player with id exists, do nothing
  if (players.find(p => p.id === id)) return;
  // Otherwise, welcome to the party
  players.push({ id, score: 0 });
}

function removePlayer(id) {
  // players is now a list without a player with id
  players = players.filter(p => p.id !== id);
}

function onWhack(id, msg) {
  // Which player just sent a whack
  let player = players.find(p => p.id === id);
  // If we have that player in our list
  // and if the incoming score is greater than their current score
  // then update that player object
  if (player && msg.score > player.score) {
    player.name = msg.player;
    player.score = msg.score;
    players[players.indexOf(player)] = player;
  }
  // Send all clients an update about the new scores
  wss.clients.forEach(client => {
    let payload = JSON.stringify({ type: "update", players });
    client.send(payload);
  });
}

wss.on("connection", socket => {
  // Create a new uid and assign it to the socket
  socket.id = uid++;
  // Add them to the player list
  addPlayer(socket.id);
  // If the player sends a whack, handle it
  socket.on("message", msg => {
    msg = JSON.parse(msg);
    if (msg.type === "whack") {
      onWhack(socket.id, msg);
    }
  });
  // When the socket closes, remove the player
  socket.on("close", () => {
    removePlayer(socket.id);
  });
});

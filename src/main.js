import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueNativeSock from "vue-native-websocket";

let wsProtocol = window.location.protocol === "https:" ? "wss:" : "ws:";

Vue.use(VueNativeSock, `${wsProtocol}//${window.location.host}`, {
  format: "json",
  reconnection: true
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

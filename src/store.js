import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    player: ""
  },
  mutations: {
    setPlayer(state, player) {
      state.player = player;
    }
  },
  actions: {}
});

import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

// Let's set up some routes. We only need two: "home" and "play"

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/play",
      name: "play",
      component: () => import(/* webpackChunkName: "play" */ "./views/Play.vue")
    }
  ]
});
